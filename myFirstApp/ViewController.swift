//
//  ViewController.swift
//  myFirstApp
//
//  Created by osx on 08/08/16.
//  Copyright © 2016 Ameba Technologies. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController
{

    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var ScoreLbl: UILabel!
   
    var count = 0
    var seconds = 0
    var timer = NSTimer()
    
    var buttonBeep : AVAudioPlayer?
    var secondBeep : AVAudioPlayer?
    var backgroundMusic : AVAudioPlayer?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        view.backgroundColor = UIColor(patternImage: UIImage(named: "bg_tile.png")!)
        
        if let buttonBeep = self.setupAudioPlayerWithFile("ButtonTap", type:"wav") {
            self.buttonBeep = buttonBeep
        }
        if let secondBeep = self.setupAudioPlayerWithFile("SecondBeep", type:"wav") {
            self.secondBeep = secondBeep
        }
        if let backgroundMusic = self.setupAudioPlayerWithFile("HallOfTheMountainKing", type:"mp3") {
            self.backgroundMusic = backgroundMusic
        }
        
        setupGame()
    }
    
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer?
    {
        //1
        let path = NSBundle.mainBundle().pathForResource(file as String, ofType: type as String)
        let url = NSURL.fileURLWithPath(path!)
        
        //2
        var audioPlayer:AVAudioPlayer?
        
        // 3
        do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }
    
    @IBAction func buttonPressed()
    {
        count = count + 1
        ScoreLbl.text = "Score: \(count)"
        buttonBeep?.play()
    }
    
    func setupGame()
    {
        backgroundMusic?.volume = 0.3
        backgroundMusic?.play()
        
        seconds = 30
        count = 0
        
        timeLbl.text = "Time :\(seconds)"
        ScoreLbl.text = "Score: \(count)"
        
        //here the timer is start
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("substrackTime"), userInfo: nil, repeats: true)
    }
    
    func substrackTime()
    {
        secondBeep?.play()
        
        seconds--
        timeLbl.text = "Time \(seconds)"
        
        if(seconds == 0)
        {
            //this line is used for invalidate the time
            
            timer.invalidate()
            
            //Set UIAlertController
            let alert = UIAlertController(title: "Time is up!", message: "You scored is \(count) points", preferredStyle: UIAlertControllerStyle.Alert)
            
            //Set the action of UIAlertController UIAlertAction
            alert.addAction(UIAlertAction(title: "Play again", style: UIAlertActionStyle.Default, handler:
                {
                
                action in self.setupGame()
            }))
            
            presentViewController(alert, animated: true, completion:nil)
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

